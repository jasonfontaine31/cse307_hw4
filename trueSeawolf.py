##########################################################################################
# Name : Jason Fontaine 
# ID   : 109605258
##########################################################################################

import ply.lex as lex

reserved = {
	'if' : 'IF',
	'else' : 'ELSE',
	'and': 'AND',
	'or' : 'OR',
	'not' : 'NOT',
	'in' : 'IN',
	'print' : 'PRINT',
	'while' : 'WHILE',
	'return' : 'RETURN'
	
}


tokens = ('INTEGER','FLOAT', 'PLUS','MINUS','TIMES','DIVIDE', 'LPAREN','RPAREN', 'LBRACK', 'RBRACK',
			'STRING' ,'COMMA','EQUALSEQ', 'NOTEQUALS', 'MOD', 'EXP', 'FDIVIDE', 
			'LT','LTEQ','GT','GTEQ','VARNAME', 'EQUALS','SEMICOLON','LCURLY','RCURLY') + tuple(reserved.values())

t_ignore = ' \t\n' 

# t_NEWLINE = r'\n'
t_PLUS    = r'\+'
t_LBRACK = r'\['
t_RBRACK = r'\]'
t_LCURLY = r'\{'
t_RCURLY = r'\}'
t_MINUS   = r'-'
t_TIMES   = r'\*'
t_DIVIDE  = r'/'
t_COMMA = r','
t_EQUALS  = r'='
t_LPAREN  = r'\('
t_RPAREN  = r'\)'

t_SEMICOLON = r';'

t_EQUALSEQ = r'=='
t_NOTEQUALS = r'<>'
t_MOD = r'%'
t_EXP = r'\*\*'
t_FDIVIDE =r'\/\/'

t_LT = r'<'
t_LTEQ = r'<='
t_GT = r'>'
t_GTEQ = r'>='

# t_IF = r'if'
# t_ELSE = r'else'
# t_AND = r'and'
# t_OR = r'or'
# t_NOT = r'not'
# t_IN = r'in'
# t_PRINT = r'print'


# t_VARNAME = r'[a-zA-Z][_a-zA-Z0-9]*'




# Tokens
# def t_ID(t):
# 	r'[a-zA-Z][a-zA-Z0-9_]*'

# 	if t.value in reserved:
# 		t.type = reserved.get(t.value) # Check for reserved words
# 	print(t)
# 	return t

def t_VARNAME(t):
	r'[a-zA-Z][_a-zA-Z0-9]*'
	t.type = reserved.get(t.value, 'VARNAME')
	return t




def t_STRING(t):
	r'\"[^\"]+\"|\"\"'
	t.value = str(t.value)[1:-1]
	return t

def t_FLOAT(t):
	r'[-]?[0-9]*\.[0-9]+'
	t.value = float(t.value)
	return t

def t_INTEGER(t):
	r'[0-9]+'
	t.value = int(t.value)
	return t



def t_error(t):
	print('t_error',t)
	raise TypeError("SYNTAX ERROR")

lexer = lex.lex()




##########################################################################################


import ply.yacc as yacc

global_variables = dict()
frame = list()
frame.append(global_variables)
# append, pop, peek


precedence = (
    ('left', 'OR'),
    ('left', 'AND'),
    ('left', 'NOT'),
    ('left', 'LT', 'LTEQ', 'GT', 'GTEQ', 'NOTEQUALS', 'EQUALSEQ'),
    ('left', 'IN'),
    ('left', 'PLUS', 'MINUS'),
    ('left', 'FDIVIDE'),
    ('left', 'EXP'),
    ('left', 'MOD'),
    ('left', 'TIMES', 'DIVIDE'),
    ('nonassoc', 'LPAREN', 'RPAREN'),
    ('nonassoc', 'LBRACK', 'RBRACK'),
    ('nonassoc', 'UMINUS')
)


def p_program(p):
	"""
	program : statements
	"""
	p[0] = run(p[1])


def p_assignment(p):
		"""
		assignment : VARNAME EQUALS expression
					| expression LBRACK expression RBRACK EQUALS expression
		"""	
		if len(p) == 4:
			p[0] = ('=',p[1],p[3])
		elif len(p) == 7:
			p[0] = ('list_index',p[1],p[3],p[6])
		# print(global_variables)

def p_block(p):
	"""
	block : LCURLY statements RCURLY
	"""
	# print('is IT EVEN GOING HERE')
	if len(p) == 4:
		p[0] = p[2]
	elif len(p) == 3:
		p[0] = None
	

def p_statments(p):
	"""
	statements : statement SEMICOLON statements
				| block statements
				| conditional statements
				| loop statements
				| define_function statements
				| empty
	"""
	# print('first statments section',p[1])
	if len(p) == 4:
		p[0] =  ('statement',p[1],p[3])
	elif len(p) == 3:
		p[0] = ('statement',p[1],p[2])
	elif len(p) == 2:
		p[0] = ('statement',p[1])
 


def p_empty(p):
	"""
	empty : 
	"""
	pass
	

def p_statement(p):
	"""
	statement : assignment
			| expression
			| print
			| return
	"""
	# print('second statment section',p[1])
	p[0] = p[1]


def p_return(p):
	"""
	return : RETURN expression
	"""
	if len(p) == 3:
		p[0] = ('return',p[2])

def p_if_conditional(p):
	"""
	conditional : IF LPAREN expression RPAREN block
				| IF LPAREN expression RPAREN statement SEMICOLON
				| IF LPAREN expression RPAREN block ELSE block
				| IF LPAREN expression RPAREN statement SEMICOLON ELSE statement SEMICOLON
	"""
	if len(p) == 6 or len(p) == 7:
		p[0] = ('if',p[3],p[5])
	elif len(p) == 8:
		p[0] = ('if_else',p[3],p[5],p[7])
	elif len(p) == 10:
		p[0] = ('if_else',p[3],p[5],p[8])

def p_loop(p):
	"""
	loop : WHILE LPAREN expression RPAREN block
	"""
	if len(p) == 6:
		p[0] = (p[1],p[3],p[5])

def p_functions(p):
	"""
	expression : function_call 
	"""
	if len(p) == 2:
		p[0] = p[1]

def p_user_function(p):
	"""
	define_function : VARNAME LPAREN parameters RPAREN block
					| VARNAME LPAREN RPAREN block
	"""
	# print('JESUS DIDN"T DIE')
	if len(p) == 6:
		p[0] = ('define_function',p[1],p[3],p[5])
	elif len(p) == 5:
		p[0] = ('define_function',p[1],[],p[4])

def p_function_call(p):
	"""
	function_call : VARNAME LPAREN param_list RPAREN 
			      | VARNAME LPAREN RPAREN 
	"""
	# print('Jesus DIED')
	if len(p) == 5:
		p[0] = ('function_call',p[1],p[3])
	elif len(p) == 4:
		p[0] = ('function_call',p[1],[])

def p_param_list(p):
	"""	
	param_list : expression 
			   |  param_list COMMA expression
	"""
	if len(p) == 2:
		p[0] = [p[1]]
	elif len(p) == 4:
		p[0] = p[1]+ [p[3]]
	

def p_parse_parameters(p):
	"""
	parameters : VARNAME COMMA parameters
				| VARNAME
	"""
	if len(p) == 2:
		p[0] = [p[1]]
	elif len(p) == 4:
		p[0] = [p[1]]+ p[3]

# def p_parse_parameter_expression(p):
# 	"""
# 	parameter_expressions : parameters COMMA VARNAME
# 				| VARNAME
# 				| empty	
# 	"""


def p_default_functions(p):
	"""
	print : PRINT LPAREN expression RPAREN
	"""
	if len(p) == 5:
		p[0] = (p[1], p[3])


def p_expression(p):
	"""
	expression : factor
	"""
	p[0] = p[1]

# def p_expression_newLine(p):
# 	"""
# 	expression : NEWLINE
# 	"""
# 	# print('Yo are you going here')
# 	pass

def p_expression_list_index(p):
	"""
	expression : expression LBRACK expression RBRACK 
	"""
	p[0] = ('index',p[1],p[3])


def p_expression_arimetic(p):
	"""
	expression : expression TIMES expression
				| expression DIVIDE expression
				| expression PLUS expression
				| expression MINUS expression
				| expression MOD expression
				| expression EXP expression
				| expression FDIVIDE expression
				| MINUS expression %prec UMINUS
	"""
	if len(p) == 4:
		p[0] = (p[2],p[1],p[3])
	elif len(p) == 3:
		p[0] = ('UMINUS',p[2])

def p_expression_boolean(p):
	"""
	expression : expression LT expression
				| expression LTEQ expression
				| expression GT expression
				| expression GTEQ expression
				| expression EQUALSEQ expression
				| expression NOTEQUALS expression
				| NOT expression
				| expression IN expression
				| expression OR expression
				| expression AND expression
				
	"""
	if len(p) == 4:
		p[0] = (p[2],p[1],p[3])
	elif len(p) == 3:
		p[0] = (p[1],p[2])

def p_expression_list(p):
	"""
	expression : LBRACK RBRACK
				| LBRACK list RBRACK
	"""

	if len(p) == 3:
		p[0] = []
	elif len(p) == 4:
		p[0] = p[2]


def p_list(p):
	"""
	list : expression 
		| list COMMA expression
	"""
	if len(p) == 2:
		p[0] = [p[1]]
	elif len(p) == 4:
		p[0] = p[1]+ [p[3]]


# def p_empty(p):
# 	"""
# 	empty :
# 	"""
# 	pass
	

def p_factor(p):
	"""
	factor : LPAREN expression RPAREN
			| FLOAT
			| INTEGER
			| STRING
			| variable
	"""
	if len(p) == 4:
		p[0] = p[2]
	elif len(p) == 2:
		p[0] = p[1]

def p_variable(p):
	"""
	variable : VARNAME
	"""
	if len(p) ==  2:
		p[0] = ('varname',p[1])
	# elif len(p) == 7:
	# 	p[0] = ('list_index',p[1],p[3],p[6])

	# if p[1] in global_variables:
	# 	p[0] = global_variables[p[1]]




# def p_expression_boolean(p):
# 	"""
# 	expression : boolean
# 	"""
# 	p[0] = p[1]

# def p_boolean(p):
# 	"""
# 	boolean : TRUE
# 		| FALSE
# 	"""
# 	if p[1] == 'True':
# 		p[0] = True
# 	elif p[1] == 'False':
# 		p[0] = False
	
returnVal = True
def run(p):
	global global_variable
	global returnVal
	if type(p) == tuple:
		# print(p)
		if(p[0]) == '+':
			return run(p[1]) + run(p[2])
		elif p[0] == '-':
			return run(p[1]) - run(p[2])
		elif p[0] == 'UMINUS':
			return - run(p[1])
		elif p[0] == '*':
			return run(p[1]) * run(p[2])
		elif p[0] == '/':
			return run(p[1]) / run(p[2])
		elif p[0] == '//':
			return run(p[1]) // run(p[2])
		elif p[0] == '**':
			return run(p[1]) ** run(p[2])
		elif p[0] == '%':
			return run(p[1]) % run(p[2])
		elif p[0] == '<':
			return int( bool( run(p[1]) < run(p[2]) ) )
		elif p[0] == '<=':
			return ( bool( run(p[1]) <= run(p[2]) ) )
		elif p[0] == '>':
			return int( bool( run(p[1]) > run(p[2]) ) )
		elif p[0] == '>=':
			return int( bool( run(p[1]) >= run(p[2]) ) )
		elif p[0] == '==':
			return int( bool( run(p[1]) == run(p[2]) ) )
		elif p[0] == '<>':
			return  int( bool( run(p[1]) != run(p[2]) ) )
		elif p[0] == 'in':
			return int( bool( run(p[1]) in run(p[2]) ) )
		elif p[0] == 'and':
			return int( bool( run(p[1])) and run(p[2]) )
		elif p[0] == 'or':
			return int( bool( run(p[1]) or run(p[2]) ) )
		elif p[0] == 'not':
			return int( bool( not run(p[1]) ) )
		elif p[0] == '=': # fam what if this is a global variable 
			if p[1] not in global_variables:
				frame[len(frame)-1][p[1]] = run(p[2])
			else:
				global_variables[p[1]] = run(p[2])
			return p[2]
		elif p[0] == 'print':
			print( run(p[1]) )
			# return ''
		elif p[0] == 'index':
			return run(p[1])[run(p[2])]
		elif p[0] == 'list_index':
			#  (p[1] == ('varname', VARNAME))
			# print('You coming here')
			if p[1][1] in frame[len(frame)-1]:
				frame[len(frame)-1][ run(p[1][1]) ][ run(p[2]) ] = run( p[3] )
			elif p[1][1] in global_variables:
				# print('var is global',run(p[2]))
				global_variables[ run(p[1][1]) ][ run(p[2]) ] = run( p[3] )
			else:
				raise TypeError('Undeclared')
		elif p[0] == 'varname': # this needs to become a while loop instead of a one check
			if p[1] in frame[len(frame)-1]:
				return frame[len(frame)-1][p[1]]
			elif p[1] in global_variables:
				return global_variables[p[1]]
			else:
				raise TypeError('Undeclared')
		elif p[0] == 'statement':
			if len(p) == 3:
				# if p[1][0] == 'return':
				# print(p[1])
				run(p[1])
				if "1__returnMe" not in frame[len(frame)-1]:
					run(p[2])
			elif len(p) == 2:
				run(p[1])
		elif p[0] == 'if':
			if run( p[1] ):
				run( p[2] )
		elif p[0] == 'if_else':
			if run( p[1] ):
				run( p[2] )
			else:
				run( p[3] )
		elif p[0] == 'while':
			while( run(p[1]) ):
				run( p[2] )
		elif p[0] == 'return':
			# print('returning', run(p[1]))

			frame[len(frame)-1]["1__returnMe"] = False

			frame[len(frame)-1]["1234_JESUS_IS_DA_BEST_HE_IS_LORD"] = run(p[1])
		elif p[0] == 'define_function':
			global_variables[p[1]] = p;
		elif p[0] == 'function_call':
			# VARNAME list 
			# 
			# print('I GUESS')
			if p[1] in global_variables:
				func = global_variables[p[1]]
				# print(func[2])
				# input()
				# print(p[2])
				# input()
				if len(func[2]) == len(p[2]):
					newFrame = dict()
					for i in range(0,len(p[2])): # loop through the params
						newFrame[func[2][i]] = run(p[2][i])

					frame.append(newFrame)
					run(func[3])
					returnMe = frame[len(frame)-1]["1234_JESUS_IS_DA_BEST_HE_IS_LORD"] if "1234_JESUS_IS_DA_BEST_HE_IS_LORD" in frame[len(frame)-1] else None
					# cprint('testing',t)
					frame.pop()
					# t = True
					# frame[len(frame)-1]["returnMe"] = True
					return returnMe

				else:
					raise ValueError("Invalid number of parameters")
			else:
				raise ValueError("Function not declared")


	else:
		return p

def p_error(p):
	# print('I dont know what you want')
	print('p_error',p)
	raise ValueError('SYNTAX ERROR')



parser = yacc.yacc()

##########################################################################################


import sys
import os.path

# import trueSeawolfParser as wolfParser
# import trueSeawolfLex as lexer


def main(argv):


	fileName = argv[1] if len(argv) > 1 else ''

	if  os.path.isfile(fileName):
		inputFile  = open(argv[1], 'r',newline=None)

	while True:
		try:
			if len(argv) < 2:
				s = input('seawolf > ')
				if len(s) == 0 or  s == '\n' or s == '\r' or s == '\r\n':
					continue
				# print(len(s),'\n' in s)
				# print(s)
				# input()
				result = parser.parse(s)
				# print(result)
			else:
				# if '\n' in data:
				# 	data =  data[0:-1]
				# print(data)
				# input()
				result = parser.parse(inputFile.read())
				break
		except EOFError :
			break
		except TypeError as error:
			print('SEMANTIC ERROR ')
			print(error)
		except ValueError as error:
			print('SYNTAX ERROR')
			print(error)
		except ZeroDivisionError as error:
			print('SEMANTIC ERROR')
			print(error)
		except IndexError as error:
			print("SEMANTIC ERROR")
			print(error)

	if os.path.isfile(fileName):
		inputFile.close()

	# data = 'if(x == 0) { print("Christ is my king"); } else { print("Jesus is Lord"); }'
	# lexer.input(data)
	# while True:
	# 	tok = lexer.token()
	# 	if not tok :
	# 		break # No more input 
	# 	print(tok)


main(sys.argv)