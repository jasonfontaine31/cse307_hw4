##########################################################################################
# Name : Jason Fontaine 
# ID   : 109605258
##########################################################################################


import ply.lex as lex

reserved = {
	'if' : 'IF',
	'else' : 'ELSE',
	# 'True' : 'TRUE',
	# 'False' : 'FALSE',
	'and': 'AND',
	'or' : 'OR',
	'not' : 'NOT',
	'in' : 'IN',
}


tokens = ('INTEGER','FLOAT', 'PLUS','MINUS','TIMES','DIVIDE', 'LPAREN','RPAREN', 'LBRACK', 'RBRACK',
			'STRING' ,'COMMA','AND','OR', 'NOT','EQUALSEQ',
			'NOTEQUALS', 'MOD', 'EXP', 'FDIVIDE', 'IN','LT','LTEQ','GT','GTEQ','NEWLINE')

precedence = (
    ('left', 'PLUS', 'MINUS'),
    ('left', 'TIMES', 'DIVIDE'),
)

t_ignore = ' \t' 
# t_STRING = r'[\"\'].*[\"\']'
t_NEWLINE = r'\n'
t_PLUS    = r'\+'
t_LBRACK = r'\['
t_RBRACK = r'\]'
t_MINUS   = r'-'
t_TIMES   = r'\*'
t_DIVIDE  = r'/'
t_COMMA = r','
# t_LIST = r'\[\]' # I NEED TO FIX THIS
# t_EQUALS  = r'='
t_LPAREN  = r'\('
t_RPAREN  = r'\)'

t_EQUALSEQ = r'=='
t_NOTEQUALS = r'<>'
t_MOD = r'%'
t_EXP = r'\*\*'
t_FDIVIDE =r'\/\/'

t_LT = r'<'
t_LTEQ = r'<='
t_GT = r'>'
t_GTEQ = r'>='


# t_VARNAME = r'[_a-zA-Z][_a-zA-Z0-9]*'



# Tokens

def t_STRING(t):
	r'[\"\'][a-zA-Z_0-9 \.]*[\"\']'
	t.value = str(t.value)[1:-1]
	return t

def t_FLOAT(t):
	r'[-]?[0-9]*\.[0-9]+'
	t.value = float(t.value)
	return t

def t_INTEGER(t):
	r'[-]?[0-9]+'
	t.value = int(t.value)
	return t

def t_ID(t):
	r'[a-zA-Z_][a-zA-Z_0-9]*'
	if t.value in reserved:
		t.type = reserved.get(t.value,'ID')    # Check for reserved words
	return t



def t_error(t):
	print("SYNTAX ERROR")


lexer = lex.lex()

if __name__ == '__main__':
	data = 'not 1 < 2'
	lexer.input(data)
	while True:
		tok = lexer.token()
		if not tok :
			break # No more input 
		print(tok)
