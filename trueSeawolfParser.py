##########################################################################################
# Name : Jason Fontaine 
# ID   : 109605258
##########################################################################################

import ply.yacc as yacc
import trueSeawolfLex
from trueSeawolfLex import tokens

global_variables = dict()


precedence = (
    ('left', 'AND'),
    ('left', 'OR'),
    ('left', 'NOT'),
    ('left', 'LT', 'LTEQ', 'GT', 'GTEQ', 'NOTEQUALS', 'EQUALSEQ'),
    ('left', 'IN'),
    ('left', 'PLUS', 'MINUS'),
    ('left', 'FDIVIDE'),
    ('left', 'EXP'),
    ('left', 'MOD'),
    ('left', 'TIMES', 'DIVIDE'),
    ('nonassoc', 'LPAREN', 'RPAREN'),
    ('nonassoc', 'LBRACK', 'RBRACK'),
    ('nonassoc', 'UMINUS')
)


def p_program(p):
	"""
	program : expression
			| assignment
	"""
	p[0] = p[1]

# def p_assignment(p):
# 		"""
# 		assignment : VARNAME EQUALS expression
# 		"""	
# 		p[0] = p[3]
# 		global_variables[p[1]] = p[3]
# 		print(global_variables)


# def p_expression(p):
# 	"""
# 	expression : factor
# 	"""
# 	p[0] = p[1]

def p_expression_newLine(p):
	"""
	expression : NEWLINE
	"""
	print('Yo are you going here')
	pass

def p_expression_list_index(p):
	"""
	expression : expression LBRACK expression RBRACK 
	"""
	p[0] = p[1][p[3]]

def p_expression_arimetic(p):
	"""
	expression : expression TIMES expression
				| expression DIVIDE expression
				| expression PLUS expression
				| expression MINUS expression
				| expression MOD expression
				| expression EXP expression
				| expression FDIVIDE expression
				| factor
				| MINUS factor %prec UMINUS
	"""
	# print('arimetic operations', len(p))
	if len(p) == 4:
		if p[2] == '+':
			p[0] = p[1] + p[3]
		elif p[2] == '-':
			p[0] = p[1] - p[3]
		elif p[2] == '*':
			p[0] = p[1] * p[3]
		elif p[2] == '/':
			# if(p[3] == 0):
				# raise E
			p[0] = p[1] / p[3]
		elif p[2] == '//':
			p[0] = p[1] // p[3]	
		elif p[2] == '**':
			p[0] = p[1] ** p[3]	
		elif p[2] == '%':
			p[0] = p[1] % p[3]
	else:
		p[0] = p[1]

def p_expression_boolean(p):
	"""
	expression : expression LT expression
				| expression LTEQ expression
				| expression GT expression
				| expression GTEQ expression
				| expression EQUALSEQ expression
				| expression NOTEQUALS expression
				| NOT expression
				| expression IN expression
				| expression OR expression
				| expression AND expression
				
	"""
	if len(p) == 4:
		if p[2] == '<':
			p[0] = 1 if p[1] < p[3] else 0
		elif p[2] == '<=':
			p[0] = 1 if p[1] <= p[3] else 0
		elif p[2] == '>':
			p[0] = 1 if p[1] > p[3] else 0
		elif p[2] == '>=':
			p[0] = 1 if p[1] >= p[3] else 0
		elif p[2] == '==':
			p[0] = 1 if p[1] == p[3] else 0
		elif p[2] == '<>':
			p[0] = 1 if p[1] != p[3] else 0
		elif p[2] == 'in':
			p[0] = 1 if p[1] in p[3] else 0
		elif p[2] == 'and':
			p[0] = 1 if p[1] and p[3] else 0
		elif p[2] == 'or':
			p[0] = 1 if p[1] or p[3] else 0
	elif len(p) == 3:
		if p[1] == 'not':
			p[0] = 1 if not p[2] else 0
	else:
		p[0] = p[1]

def p_expression_list(p):
	"""
	expression : LBRACK RBRACK
				| LBRACK list RBRACK
	"""

	if len(p) == 3:
		p[0] = []
	elif len(p) == 4:
		p[0] = p[2]


def p_list(p):
	"""
	list : expression 
		| list COMMA expression
	"""
	if len(p) == 2:
		p[0] = [p[1]]
	elif len(p) == 4:
		p[0] = p[1]+ [p[3]]


# def p_empty(p):
# 	"""
# 	empty :
# 	"""
# 	p[0] = []
	

def p_factor(p):
	"""
	factor : LPAREN expression RPAREN
			| FLOAT
			| INTEGER
			| STRING
	"""
	if len(p) == 4:
		p[0] = p[2]
	elif len(p) == 2:
		p[0] = p[1]

# def p_variable(p):
# 	"""
# 	variable : VARNAME
# 	"""
# 	if p[1] in global_variables:
# 		p[0] = global_variables[p[1]]




# def p_expression_boolean(p):
# 	"""
# 	expression : boolean
# 	"""
# 	p[0] = p[1]

# def p_boolean(p):
# 	"""
# 	boolean : TRUE
# 		| FALSE
# 	"""
# 	if p[1] == 'True':
# 		p[0] = True
# 	elif p[1] == 'False':
# 		p[0] = False
	


def p_error(p):
	print('SEMANTIC ERROR')


parser = yacc.yacc()






